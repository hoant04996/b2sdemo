***Variables***
${onePgSuccess_orderNumber_guest}   xpath://span[@class='order-number']
${onePgSuccess_orderNumber_customer}   xpath://a[@class='order-number']/strong
${onePgSuccess_title}    xpath://h1[@class='page-title']/span
${onePgSuccess_delivered}    xpath://p[@class='delivery-message']/span
${onePgSuccess_emailLink}    xpath://a[@class='customer-email']
${onePgSuccess_email}    xpath:(//div[@class='messages']/..//span)[2]