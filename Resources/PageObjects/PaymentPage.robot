***Variables***
${paymentPg_masterpass}     xpath://input[@id='credit_card_number']
${paymentPg_creditCardHolderName}     xpath://input[@id='credit_card_holder_name']
${paymentPg_creditCardExpiryMonth}     xpath://select[@id='credit_card_expiry_month']
${paymentPg_creditCardExpiryYear}     xpath://select[@id='credit_card_expiry_year']
${paymentPg_creditCardCvv}     xpath://input[@id='credit_card_cvv']
${paymentPg_creditCardIssuingBankCountry}     xpath://select[@id='credit_card_issuing_bank_country']
${paymentPg_creditCardIssuingBankDdl}     xpath://select[@id='credit_card_issuing_bank_ddl']\
${paymentPg_cardholderEmail}     xpath://input[@id='cardholder_email']
${paymentPg_continuePayment}      xpath://input[@id='btnCCSubmit']
