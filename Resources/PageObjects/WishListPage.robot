***Variables***
${wishListPg_quantity}    xpath:(//input[@class='input-text qty'])[1]
${wishListPg_btnPlus}    xpath:(//button[@class='btn-plus'])[1]
${wishListPg_btnMinus}    xpath:(//button[@class='btn-minus'])[1]
${wishListPg_updateWishList}    xpath://button[@class='action update']
${wishListPg_addAlloCart}    xpath://button[@class='action tocart']
${wishListPg_emptyMessage}    xpath://div[@class='message info empty']/span